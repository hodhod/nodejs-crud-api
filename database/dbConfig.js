const mongoose = require('mongoose')

const connOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // useCreateIndex: true
}
const MONGO_URI = process.env.MONGO_URI || 'mongodb+srv://m001-student:m001-mongodb-basics@sandbox.ywn6w.mongodb.net/database?retryWrites=true&w=majority'

//m001-mongodb-basics
const connectToDB = async () => {
  try {
    await mongoose.connect(
      MONGO_URI,
      { useNewUrlParser: true, useUnifiedTopology: true },
      () => console.log(" Mongoose is connected")
    )
  } catch (err) {
    console.log(`Database error ${err}`)
  }
}


module.exports = connectToDB
