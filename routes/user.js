const express = require('express')
const { check } = require('express-validator')

const userController = require('../controllers/user')

const router = express.Router()

router.post(
  '/signup',
  [
    check('name', 'Please enter a valid name').trim().notEmpty(),
    check('email', 'Please enter a valid email').trim().isEmail(),
    check('password', 'Please enter a valid password')
      .trim()
      .isLength({ min: 6 }),
    check('role', 'Please enter a valid role').trim().notEmpty(),
  ],
  userController.signUp
)

router.post(
  '/login',
  [
    check('email', 'Please enter a valid email').trim().isEmail(),
    check('password', 'Please enter a valid password')
      .trim()
      .isLength({ min: 6 }),
  ],
  userController.login
)

module.exports = router
