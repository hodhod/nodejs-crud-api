const User = require('../models/userModel')
const bcrypt = require('bcrypt')
const { validationResult } = require('express-validator')

const { createToken } = require('../utils/jwt')
const CustomError = require('../models/CustomError')

const signUp = async (req, res, next) => {
  console.log('this is the req.body', req.body)
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({
      success: false,
      errors: errors.array(),
    })
  }

  const { name, email, password, role } = req.body
  try {
    let user = await User.findOne({ email })
    if (user) {
      console.log('---------- already exists')
      return res.status(403).json({
        code: 403,
        message: 'User with provided username already exists',
      })

      // return next(
      //   new CustomError('User with provided email already exists', 403)
      // )
    }

    // Since username should also be unique
    // user = await User.findOne({ username })

    // if (user) {
    //   return next(
    //     new CustomError('User with provided username already exists', 403)
    //   )
    // }

    user = new User({
      name,
      email,
      password,
      role
    })

    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(password, salt)

    user.password = hashedPassword
    await user.save()

    res.status(200).json({
      success: true,
      msg: "Registered Successfully!",
      user: user
    })
  } catch (err) {
    next(new CustomError('Something went wrong', 500))
  }
}

const login = async (req, res, next) => {
  console.log('this is the req.body', req.body)
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    console.log('1')
    return res.status(400).json({
      success: false,
      errors: errors.array(),
    })
  }
  const { email, password } = req.body
  try {
    let user = await User.findOne({ email })

    if (!user){
      console.log('2')
      return res.status(400).json({
        code:400,
        message: 'Invalid credentials'
      })
    } 

    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
      console.log('3')
      return res.status(400).json({
        code:400,
        message: 'Invalid credentials'
      })
    }

    const token = createToken({
      id: user._id,
    })
    user.isLoggedIn = true
    await user.save()
    console.log('4')
    res
      .header('authorization', token)
      .send({ 
        msg: 'Successfully', 
        token, 
        user 
      })
  } catch (err) {
    console.log(err)
    console.log('5')
    return 
        res.status(500).json({
          code:500,
          message: 'Something went wrong'
        })
  }
}

module.exports = { signUp, login }
